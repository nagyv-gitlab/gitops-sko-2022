# gitops-sko-2022

This lab will guide you through the steps to create, configure and deploy your own GitLab agent for K8s to a running GKE cluster. During the lab, you will be modifying and adding files to the repository. To do so, you can either clone the repo to your laptop, or directly use the Web IDE on your browser.

## Pre-requisites

In order to take part in the hands-on session, you'll need a few applications installed on your laptop:

- [gcloud CLI](https://cloud.google.com/sdk/docs/install). **NOTE: you may need to run `gcloud init` and/or `gcloud auth login` to get gcloud running properly on your laptop.**
- [docker or alternative](https://rancherdesktop.io/) - for more information on alternative check our [handbook page](https://about.gitlab.com/handbook/tools-and-tips/mac/#docker-desktop)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)
- [kpt](https://googlecontainertools.github.io/kpt/installation/) - Optional
- [kustomize](https://kubectl.docs.kubernetes.io/installation/kustomize/) - Optional
- [k9s](https://github.com/derailed/k9s) / [Lens](https://k8slens.dev/) - Optional
- You should have created a merge request (MR) for the creation of your personalized namespace per the instructions in the following [issue](https://gitlab.com/nagyv-gitlab/gitops-sko-2022/-/issues/1). This MR must contain ONLY the updates in the instructions and nothing else. This MR will be merged by the instructor during the lab. **PLEASE DO NOT MERGE THIS MR TO THE MAIN BRANCH.**

You will have been added as a Kubernetes Engine Admin to GCP ahead of this lab. We will share the kubeconfig command to run in order to access the running GKE cluster during the lab. In addition, you will have been added to this project as a Maintainer.

## Hands-on lab steps

### Checking connectivity to GKE from your laptop

1. Open a Terminal window on your laptop and enter the kubeconfig command that will be shared with you during the lab to connect to the running GKE. The command will look like this (during the lab, it will be different):

```
gcloud container clusters get-credentials <CLUSTER NAME> --zone <GCP ZONE> --project <GCP PROJECT NAME>
```

The command above will add an entry to your `${HOME}.kube/config` file on your laptop so that you can execute kubectl commands against the running GKE, for example.

2. Check connection to the GKE cluster by entering the following command from your Terminal window:

```
kubectl config get-contexts
kubectl get pods --all-namespaces
```

> NOTE: At this point, you could have a look at the cluster using the lens or k9s tools, if you have them installed and configured on your laptop.

### Create a merge request for configuring your own agent

3. Head over to project **https://gitlab.com/nagyv-gitlab/gitops-sko-2022** and create an MR that creates an agent config file `.gitlab/agents/<your handle>/config.yaml`. You can use the Web IDE to do this.

4. Paste the following content into the newly created file:

> NOTE: replace <your handle> with your GitLab handle. Leave the rest of the text the same. The manifest path should point directly to the participant's namespace file.

<pre>
gitops:
  manifest_projects:
  - id: nagyv-gitlab/gitops-sko-2022
    default_namespace: gitlab-kubernetes-agent
    paths:
      # Change the following line
    - glob: 'manifests/participant-namespaces/<mark><b>&lt;your handle&gt;</b></mark>.yaml'
</pre>

5. Commit the updates to the specific MR feature branch for these changes. Enter the words "Agent configuration" in the MR Commit message. **NOTE: Make sure that this is a BRAND NEW MR. DO NOT add these updates to the MR from the pre-requisite section. This MR must contain ONLY the updates from step 4. PLEASE DO NOT MERGE THIS MR TO THE MAIN BRANCH.**

6. At this point, ensure that the instructor merges your MR above to the main branch.

### Registering your agent with GitLab

7. Now, you need to register the agent. Back in project **https://gitlab.com/nagyv-gitlab/gitops-sko-2022**, select **Infrastructure > Kubernetes clusters** from the left vertical menu.

8. From the Kubernetes window, click on the **Install a new agent** button on the middle right side of the screen. **NOTE: If the button is greyed out, ask the instructor to make you a maintainer in the project.**

9. From the pop-down list, select the agent with your GitLab handle as its name. Then click on the **Next** button.

10. The next screen will show a `docker run ... | kubectl apply` command. **Copy the command and save it to a local file on your laptop.** This command will install your GitLab Agent on the running GKE cluster.

11. Add the option `--name-prefix <your GitLab handle>` to the previous command as follows:

<pre>
docker run --pull=always --rm \
    registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate \
    --agent-token=xxxxxxxxxxxx \
    --kas-address=wss://kas.gitlab.com \
    --agent-version stable \
    --namespace gitlab-kubernetes-agent <mark><b>--name-prefix &lt;your handle&gt;</b></mark> | kubectl apply -f -
</pre>

### Deploying your agent to GKE

12. Execute the updated `docker run ... | kubectl apply` command in a Terminal window on your laptop.

13. Get the name of the pod for your agent with the following command:

```
kubectl get pods --namespace gitlab-kubernetes-agent
```

> NOTE: identify your agent pod!

14. Then, start trailing your agent's log output by entering the following command, where you need to specify the name of the pod for your agent:

<pre>
kubectl logs -f <mark><b>&lt;name of the pod for your agent&gt;</b></mark> --namespace gitlab-kubernetes-agent
</pre>

### Have the agent create your namespace in the GKE cluster

13. At this point, ensure that the instructor merges the MR with the feature branch for the creation of your personalized namespace that was part of the pre-requisite of this lab.

14. Keep an eye on the Terminal window where you are trailing the log output of your agent. You should see the agent detect the new `<your handle>.yaml` file in `manifests/participant-namespaces` and apply the contents of the file to the GKE cluster, effectively creating your new personalized namespace.

15. To verify that your personalized namespace was created by the agent, enter the following command from a Terminal window:

```
kubectl get namespace
```

You should see your personalized namespace in the list of namespaces.

Congratulations! You have completed this lab.
